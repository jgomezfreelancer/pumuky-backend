import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { UserRepository } from 'src/auth/user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { ReportUserDto } from './dto/report-user.dto';
import { User } from 'src/auth/user.entity';

@Injectable()
export class EmailService {
  constructor(
    private readonly mailerService: MailerService,
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  renderBody(user: string, password: string): string {
    const html = `
            <html>
            <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
              <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                  <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">
        
                    <!-- CENTERED WHITE CONTAINER -->
                    <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
        
                      <!-- MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                            <tr>
                              <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Recuperación de contraseña</p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Usuario ${user}, se ha generado la siguiente contraseña para que puedas iniciar sesión: <strong>${password}<strong></p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
        
                    <!-- FOOTER -->
                    <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                          <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                            <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Help App Inc</span>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </body>
        </html>
        `;
    return html;
  }

  renderUserReportBody(
    user: User,
    userReport: User,
    reason: string,
    baseUrl: string,
  ): string {
    const userName = `${user.first_name} ${
      user.last_name ? user.last_name.charAt(0) : ''
    } `;

    const userReportName = `${userReport.first_name} ${
      userReport.last_name ? userReport.last_name.charAt(0) : ''
    } `;
    // const image = `${baseUrl}/logo.svg`;
    //   <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
    //   <img src="https://745999091093.ngrok.io/logo.svg" width="100" height="100">
    // </td>
    const html = `
            <html>
            <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
              <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                  <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">
        
                    <!-- CENTERED WHITE CONTAINER -->
                    <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
        
                      <!-- MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                            <tr>
                              <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><strong><h1>Sistema de ayuda Bowi<h2></strong></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><strong><h2>Denuncia de usuario<h2></strong></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">El Usuario <strong>${userName}</strong>, ha realizado una denuncia contra el usuario <strong>${userReportName}</strong></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><strong>Motivo<strong>: ${reason}</strong></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; margin-top: 10px"><strong><h3>Detalles de usuario reportado<h3></strong></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><strong>Nombre y Apellido:</strong> ${
                                  userReport.first_name
                                } ${
      userReport.last_name ? userReport.last_name : ''
    }</p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><strong>Correo:</strong> ${
                                  userReport.username
                                }</p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
        
                    <!-- FOOTER -->
                    <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                          <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                            <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Help App Inc</span>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </body>
        </html>
        `;
    return html;
  }

  async sendMail(to: string): Promise<{ status: boolean; message: string }> {
    const user = await this.userRepository.findOne({ where: { username: to } });
    if (user) {
      const randomPassword = Math.random()
        .toString(36)
        .slice(-8);
      const salt = await bcrypt.genSalt();
      const password = await this.userRepository.hashPassword(
        randomPassword,
        salt,
      );
      await this.userRepository.update(user.id, { salt, password });
      return this.mailerService
        .sendMail({
          to,
          from: 'noreply@noreply.com',
          subject: 'Pumuky App - Recuperar contraseña',
          html: this.renderBody(to, randomPassword),
        })
        .then(res => {
          return { status: true, message: 'Correo enviado' };
        })
        .catch(() => {
          return {
            status: false,
            message: 'Error en el correo',
          };
        });
    } else {
      return { status: false, message: 'Correo no existe' };
    }
  }

  async sendReportMail(
    body: ReportUserDto,
    baseUrl: string,
  ): Promise<{ status: boolean; message: string }> {
    const { authUser, reportUser, reason } = body;
    const user = await this.userRepository.findOne(authUser);
    const complainUser = await this.userRepository.findOne(reportUser);

    return this.mailerService
      .sendMail({
        to: 'sikerborr@gmail.com',
        from: 'noreply@noreply.com',
        subject: 'Bowi - Denuncia de usuario',
        html: this.renderUserReportBody(user, complainUser, reason, baseUrl),
      })
      .then(res => {
        return { status: true, message: 'correoEnviado' };
      })
      .catch(() => {
        return {
          status: false,
          message: 'correoError',
        };
      });
  }
}
