import { extname } from 'path';

export const getImageName = (image: string, id: number) => {
  const name = image.split('.')[0];
  const fileExtName = extname(image);
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  return `${id}-${name}-${randomName}${fileExtName}`;
};
