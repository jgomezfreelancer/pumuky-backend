import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AnimalRepository } from 'src/animal/animal.repository';
import { Care } from './care.entity';
import { CareRepository } from './care.repository';
import { CreateCareDto } from './dto/create-care.dto';
import { DeleteCareDto } from './dto/delete-care.dto';

@Injectable()
export class CareService {
  constructor(
    @InjectRepository(CareRepository)
    private repo: CareRepository,
    @InjectRepository(AnimalRepository)
    private animalRepo: AnimalRepository,
  ) { }

  async store(body: CreateCareDto): Promise<Care[]> {
    const { description, note, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    const vaccine = new Care();
    vaccine.description = description;
    vaccine.note = note;
    vaccine.animal = animal;
    await vaccine.save();
    return this.repo.find({ where: { animal } });
  }

  async update(id: number, body: CreateCareDto): Promise<Care[]> {
    const { description, note, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    const data = {
      description,
      note,
    };
    await this.repo.update(id, data);
    const cares = await this.repo.find({ where: { animal } });
    return cares;
  }

  async delete(body: DeleteCareDto): Promise<Care[]> {
    const { id, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
    const cares = await this.repo.find({ where: { animal } });
    return cares;
  }

  async updateCareChecked(body: {
    id: number;
    animalId: number;
    date: string;
  }): Promise<Care[]> {
    const { id, animalId, date } = body;
    const animal = await this.animalRepo.findOne(animalId);
    await this.repo.update(id, { checked_date: date });
    const cares = await this.repo.find({ where: { animal } });
    return cares;
  }
}
