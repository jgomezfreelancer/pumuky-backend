import { Animal } from 'src/animal/animal.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Care extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true })
  description: string;

  @Column('simple-array', { default: null, nullable: true })
  note: number[];

  @Column({ type: 'date', default: null, nullable: true })
  checked_date: string;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @ManyToOne(
    () => Animal,
    animal => animal.animalCare,
  )
  animal: Animal;
}
