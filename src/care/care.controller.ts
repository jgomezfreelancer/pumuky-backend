import { Body, Controller, Param, Post, Put } from '@nestjs/common';
import { Care } from './care.entity';
import { CareService } from './care.service';
import { CreateCareDto } from './dto/create-care.dto';
import { DeleteCareDto } from './dto/delete-care.dto';

@Controller('care')
export class CareController {
  constructor(private service: CareService) {}
  @Post('/')
  async store(@Body() body: CreateCareDto): Promise<Care[]> {
    return this.service.store(body);
  }

  @Put('/:id')
  async update(
    @Body() body: CreateCareDto,
    @Param('id') id: number,
  ): Promise<Care[]> {
    return this.service.update(id, body);
  }

  @Post('/delete')
  async delete(@Body() body: DeleteCareDto): Promise<Care[]> {
    return this.service.delete(body);
  }

  @Post('/checked')
  async updateCareChecked(
    @Body() body: { id: number; animalId: number; date: string },
  ): Promise<Care[]> {
    return this.service.updateCareChecked(body);
  }
}
