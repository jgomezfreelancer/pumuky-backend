import { Module } from '@nestjs/common';
import { CareController } from './care.controller';
import { CareService } from './care.service';
import { CareRepository } from './care.repository';
import { AnimalRepository } from 'src/animal/animal.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([CareRepository, AnimalRepository])],
  controllers: [CareController],
  providers: [CareService],
})
export class CareModule {}
