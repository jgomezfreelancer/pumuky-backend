import { EntityRepository, Repository } from 'typeorm';
import { Care } from './care.entity';

@EntityRepository(Care)
export class CareRepository extends Repository<Care> {}
