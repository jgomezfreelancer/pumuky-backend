import { IsNotEmpty } from 'class-validator';

export class DeleteCareDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  animalId: number;
}
