import { IsNotEmpty } from 'class-validator';

export class CreateCareDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  note: number[];

  @IsNotEmpty()
  animalId: number;
}
