import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateReminderDto } from './dto/create-reminder.dto';
import { UpdateReminderDto } from './dto/update-reminder.dto';
import { Reminder } from './reminder.entity';
import { ReminderService } from './reminder.service';

@Controller('reminder')
export class ReminderController {
  constructor(private service: ReminderService) { }

  @Get('/:id')
  async get(@Param('id') id: number): Promise<Reminder> {
    return this.service.get(id);
  }

  @Put('/:id')
  async update(
    @Param('id') id: number,
    @Body() body: UpdateReminderDto,
  ): Promise<Reminder> {
    return this.service.update(id, body);
  }

  @Post('/')
  async store(@Body() body: CreateReminderDto): Promise<Reminder[]> {
    return this.service.store(body);
  }

  @Delete('/:id')
  async delete(@Param('id') id: number): Promise<any> {
    return this.service.delete(id);
  }

  @Get('/notification/:id')
  async notificationExample(@Param('id') id: number): Promise<any> {
    return this.service.notificationTest(id);
  }

  @Post('/cron-example')
  async cronExample(): Promise<any> {
    return this.service.handleCronCalendar();
  }
}
