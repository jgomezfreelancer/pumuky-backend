import { IsNotEmpty } from 'class-validator';

export class CreateReminderDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  location: string;

  @IsNotEmpty()
  start: string;

  @IsNotEmpty()
  end: string;

  @IsNotEmpty()
  isAlert: boolean;

  @IsNotEmpty()
  calendarId: number;

  @IsNotEmpty()
  calendarDate: string;

  @IsNotEmpty()
  userId: number;
}
