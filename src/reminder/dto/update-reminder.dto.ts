import { IsNotEmpty } from 'class-validator';

export class UpdateReminderDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  start: string;

  @IsNotEmpty()
  end: string;

  @IsNotEmpty()
  isAlert: boolean;

  @IsNotEmpty()
  userId: number;
}
