import { Animal } from 'src/animal/animal.entity';
import { UserCalendar } from 'src/user-calendar/user-calendar.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Reminder extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true })
  description: string;

  @Column({ default: null, nullable: true })
  location: string;

  @Column({ default: null, nullable: true })
  start: string;

  @Column({ default: null, nullable: true })
  end: string;

  @Column({ default: null, nullable: true })
  is_alert: boolean;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @Column({ default: null, nullable: true })
  userCalendarId: boolean;

  @ManyToOne(
    () => UserCalendar,
    animal => animal.reminders,
  )
  userCalendar: UserCalendar;
}
