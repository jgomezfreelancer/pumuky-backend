import { Module } from '@nestjs/common';
import { ReminderService } from './reminder.service';
import { ReminderController } from './reminder.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReminderRepository } from './reminder.repository';
import { UserCalendarRepository } from 'src/user-calendar/user-calendar.repository';
import { UserRepository } from 'src/auth/user.repository';
import { UserCalendarService } from 'src/user-calendar/user-calendar.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ReminderRepository,
      UserCalendarRepository,
      UserRepository,
    ]),
  ],
  providers: [ReminderService, UserCalendarService],
  controllers: [ReminderController],
})
export class ReminderModule {}
