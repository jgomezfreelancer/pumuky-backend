import { Injectable, Logger, Post, } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import e from 'express';
import * as admin from 'firebase-admin';
import * as moment from 'moment';
import 'moment-timezone'

import { UserRepository } from 'src/auth/user.repository';
import { UserCalendarRepository } from 'src/user-calendar/user-calendar.repository';
import { UserCalendarService } from 'src/user-calendar/user-calendar.service';
import { CreateReminderDto } from './dto/create-reminder.dto';
import { UpdateReminderDto } from './dto/update-reminder.dto';
import { Reminder } from './reminder.entity';
import { ReminderRepository } from './reminder.repository';

@Injectable()
export class ReminderService {
  private logger = new Logger('AuthService');
  constructor(
    @InjectRepository(ReminderRepository)
    private repo: ReminderRepository,
    @InjectRepository(UserCalendarRepository)
    private userCalendarRepo: UserCalendarRepository,
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    private userCalendarService: UserCalendarService,
  ) { }

  async get(id: number): Promise<Reminder> {
    return this.repo.findOne(id);
  }

  async update(id: number, body: UpdateReminderDto): Promise<Reminder> {
    const { description, start, end, isAlert } = body;
    await this.repo.update(id, {
      description,
      start: start,
      end: end,
      is_alert: isAlert
    });
    return this.repo.findOne(id);
  }

  async store(body: CreateReminderDto): Promise<Reminder[]> {
    const {
      description,
      start,
      end,
      isAlert,
      calendarId,
      userId,
      calendarDate,
    } = body;
    const user = await this.userRepo.findOne(userId);
    let calendar = await this.userCalendarRepo.findOne({
      where: { user, date: calendarDate },
    });

    if (!calendar) {
      calendar = await this.userCalendarService.store({
        date: calendarDate,
        userId,
      });
    }
    const reminder = new Reminder();
    reminder.description = description;
    reminder.start = start;
    reminder.end = end;
    reminder.is_alert = isAlert;
    reminder.userCalendar = calendar;
    await reminder.save();
    return this.repo.find({ where: { userCalendar: calendar } });
  }

  async getAllByCalendar(calendarId: number): Promise<Reminder[]> {
    const calendar = await this.userCalendarRepo.findOne(calendarId);
    return this.repo.find({ where: { userCalendar: calendar } });
  }

  async delete(id: number): Promise<any> {
    return this.repo.delete(id);
  }

  async notificationTest(userId: number): Promise<any> {
    const user = await this.userRepo.findOne(userId)
    const dataNotification = {
      id: user.id,
      type: 'accept',
      title: 'test',
      body: 'example body',
      user: {},
      smallIcon: 'drawable/icon',
      largeIcon: 'https://avatars2.githubusercontent.com/u/1174345?v=3&s=96',
      autoCancel: true,
      vibrate: [200, 300, 200, 300],
      color: '0000ff',
      headsUp: true,
      sound: true,
    };

    const payload = {
      data: {
        notification: JSON.stringify({
          ...dataNotification,
          user: {},
        }),
        priority: 'high',
      },
      notification: {
        title: 'test title',
        body: 'body example',
        content: JSON.stringify(dataNotification),
      },
    };

    await admin
      .messaging()
      .sendToDevice(user.fcm, payload)
      .then(res => {
        if (res.failureCount === 0) {
          this.logger.debug(
            `Firebase Send Push Notification to user ${user.username} type accept`,
          );
        } else {
          this.logger.debug(
            `Firebase Error to Sending Push Notification to user ${user.username
            },  error : ${JSON.stringify(res.results[0])}`,
          );
        }
      }).catch(err => err);

  }


  async sendNotfication(userId: number, title: string): Promise<any> {
    const user = await this.userRepo.findOne(userId)
    const dataNotification = {
      id: user.id,
      type: 'accept',
      title,
      body: '',
      user: {},
      smallIcon: 'drawable/icon',
      largeIcon: 'https://avatars2.githubusercontent.com/u/1174345?v=3&s=96',
      autoCancel: true,
      vibrate: [200, 300, 200, 300],
      color: '0000ff',
      headsUp: true,
      sound: true,
    };

    const payload = {
      data: {
        notification: JSON.stringify({
          ...dataNotification,
          user: {},
        }),
        priority: 'high',
      },
      notification: {
        title,
        body: '',
        content: JSON.stringify(dataNotification),
      },
    };

    if (user.fcm) {
      await admin
        .messaging()
        .sendToDevice(user.fcm, payload)
        .then(res => {
          if (res.failureCount === 0) {
            this.logger.debug(
              `Firebase Send Push Notification to user ${user.username} type schedule`,
            );
          } else {
            this.logger.debug(
              `Firebase Error to Sending Push Notification to user ${user.username
              },  error : ${JSON.stringify(res.results[0])}`,
            );
          }
        }).catch(err => err);
    } else {
      this.logger.debug(
        `FCM ERROR - FAIL Firebase Send Push Notification to user ${user.username} type schedule`,
      );
    }



  }

  @Cron('10 * * * * *')
  async handleCronCalendar(): Promise<void> {
    const calendars = await this.userCalendarRepo.find({ relations: ['user', 'reminders'] });
    for (const item of calendars) {
      const user = item.user;
      if (user && user.timezone && item.date) {
        const currentTimezone = moment().tz(user.timezone).format('YYYY-MM-DD');
        const calendarDaybefore = moment(item.date).tz(user.timezone).subtract(1, 'day').format('YYYY-MM-DD');
        /* validation de before day notification */
        if (currentTimezone === calendarDaybefore && item.reminders && item.reminders.length > 0 && !item.day_before) {
          await this.userCalendarRepo.update(item.id, { day_before: true })
          this.sendNotfication(user.id, 'Tienes recordatorios para mañana')
        }
        else if (currentTimezone === item.date && item.reminders && item.reminders.length > 0 && !item.notification_send) {
          await this.userCalendarRepo.update(item.id, { notification_send: true })
          this.sendNotfication(user.id, 'Tienes recordatorios para hoy')
        } else {

        }
      }


    }
  }

}
