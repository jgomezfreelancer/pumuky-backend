import { EntityRepository, Repository } from 'typeorm';
import { Reminder } from './reminder.entity';

@EntityRepository(Reminder)
export class ReminderRepository extends Repository<Reminder> {}
