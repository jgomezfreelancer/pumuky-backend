import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { AnimalAge } from './animal-age.entity';
import { CreateAnimalAgeDto } from './dto/create-animal-age.dto';

@EntityRepository(AnimalAge)
export class AnimalAgeRepository extends Repository<AnimalAge> {
  async store(body: CreateAnimalAgeDto): Promise<AnimalAge> {
    const { description } = body;

    const nationality = new AnimalAge();
    nationality.description = description;
    try {
      return nationality.save();
    } catch (error) {
      if (error.code === '23505') {
        // duplicate
        throw new ConflictException('AnimalAge already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
