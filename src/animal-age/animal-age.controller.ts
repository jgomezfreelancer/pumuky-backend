import { Body, Controller, Get, Post } from '@nestjs/common';
import { AnimalAge } from './animal-age.entity';
import { AnimalAgeService } from './animal-age.service';
import { CreateAnimalAgeDto } from './dto/create-animal-age.dto';

@Controller('animal-age')
export class AnimalAgeController {
  constructor(private service: AnimalAgeService) {}

  @Get('/')
  async index(): Promise<AnimalAge[]> {
    return this.service.index();
  }

  @Post('/')
  async store(@Body() body: CreateAnimalAgeDto): Promise<AnimalAge> {
    return this.service.store(body);
  }
}
