import { Test, TestingModule } from '@nestjs/testing';
import { AnimalAgeService } from './animal-age.service';

describe('AnimalAgeService', () => {
  let service: AnimalAgeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnimalAgeService],
    }).compile();

    service = module.get<AnimalAgeService>(AnimalAgeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
