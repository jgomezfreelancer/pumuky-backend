import { IsNotEmpty } from 'class-validator';

export class CreateAnimalAgeDto {
  @IsNotEmpty()
  description: string;
}
