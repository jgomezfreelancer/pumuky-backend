import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AnimalAge } from './animal-age.entity';
import { AnimalAgeRepository } from './animal-age.repository';
import { CreateAnimalAgeDto } from './dto/create-animal-age.dto';

@Injectable()
export class AnimalAgeService {
  constructor(
    @InjectRepository(AnimalAgeRepository)
    private repo: AnimalAgeRepository,
  ) {}

  async index(): Promise<AnimalAge[]> {
    return this.repo.find({ order: { description: 'ASC' } });
  }

  async store(body: CreateAnimalAgeDto): Promise<AnimalAge> {
    return this.repo.store(body);
  }
}
