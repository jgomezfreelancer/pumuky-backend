import { Test, TestingModule } from '@nestjs/testing';
import { AnimalAgeController } from './animal-age.controller';

describe('AnimalAgeController', () => {
  let controller: AnimalAgeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnimalAgeController],
    }).compile();

    controller = module.get<AnimalAgeController>(AnimalAgeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
