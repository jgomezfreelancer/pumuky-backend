import { Module } from '@nestjs/common';
import { AnimalAgeService } from './animal-age.service';
import { AnimalAgeController } from './animal-age.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnimalAgeRepository } from './animal-age.repository';

@Module({
  imports: [TypeOrmModule.forFeature([AnimalAgeRepository])],
  providers: [AnimalAgeService],
  controllers: [AnimalAgeController],
})
export class AnimalAgeModule {}
