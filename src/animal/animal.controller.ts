import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { Animal } from './animal.entity';
import { AnimalService } from './animal.service';
import { CreateAnimalDto } from './dto/create-animal.dto';
import { DeleteAnimalDto } from './dto/delete-animal.dto';
import { UploadAnimalImageDto } from './dto/upload-animal-image.dto';

@Controller('animal')
export class AnimalController {
  constructor(private service: AnimalService) {}

  @Get('/user/:id')
  async getAnimalByUser(@Param('id') id: number): Promise<Animal[]> {
    return this.service.getAnimalByUser(id);
  }

  @Get('/:id')
  async get(@Param('id') id: number): Promise<Animal> {
    return this.service.get(id);
  }

  @Post('/image')
  async getImage(@Body() body: { image: string }): Promise<string> {
    return this.service.getAnimalImage(body.image);
  }

  @Put('/:id')
  async update(
    @Body() body: CreateAnimalDto,
    @Param('id') id: number,
  ): Promise<Animal> {
    return this.service.update(body, id);
  }

  @Post('/upload-image')
  async uploadImage(@Body() body: UploadAnimalImageDto): Promise<Animal> {
    return this.service.uploadImage(body);
  }

  @Post('/')
  async store(@Body() body: CreateAnimalDto): Promise<Animal> {
    return this.service.store(body);
  }

  @Post('/delete')
  async delete(@Body() body: DeleteAnimalDto): Promise<Animal[]> {
    return this.service.removeAnimal(body);
  }
}
