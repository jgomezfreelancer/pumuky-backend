import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Animal } from './animal.entity';
import { CreateAnimalDto } from './dto/create-animal.dto';
import { User } from 'src/auth/user.entity';
import { UploadAnimalImageDto } from './dto/upload-animal-image.dto';
import * as fs from 'fs';
import { extname } from 'path';
import { getImageName } from 'src/utils/helpers';

@EntityRepository(Animal)
export class AnimalRepository extends Repository<Animal> {
  async uploadImage(
    id: number,
    file: {
      data?: string;
      type?: string;
      name?: any;
    },
  ): Promise<any> {
    if (file) {
      const currentAnimal = await this.findOne(id);
      const fileExtName = extname(file.name);
      const image = getImageName(file.name, id);
      // if (currentAnimal.image) {
      //   await fs.unlinkSync(`src/images/${currentAnimal.image}`);
      // }
      const res = await fs.writeFile(
        `src/images/${image}`,
        file.data,
        'base64',
        function(err) {},
      );
      this.update(id, { image });
    }
  }

  async store(body: CreateAnimalDto, user: User): Promise<Animal> {
    const {
      name,
      description,
      age,
      reception_date,
      food,
      suplements,
      maintenance,
      amount_schedule,
      notes,
      race,
      fileImage,
    } = body;

    const animal = new Animal();
    animal.name = name;
    animal.description = description;
    animal.age = age ? age : null;
    animal.reception_date = reception_date ? reception_date : null;
    animal.race = race;
    animal.food = food;
    animal.suplements = suplements;
    animal.maintenance = maintenance;
    animal.amount_schedule = amount_schedule;
    animal.notes = notes;
    animal.user = user;
    try {
      const entity = await animal.save();
      if (fileImage) {
        await this.uploadImage(entity.id, fileImage);
      }
      return this.findOne(entity.id);
    } catch (error) {
      if (error.code === '23505') {
        // duplicate
        throw new ConflictException('Animal already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
