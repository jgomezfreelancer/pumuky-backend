import { AnimalUser } from 'src/animal-user/animal-user.entity';

import { AnimalWeight } from 'src/animal-weight/animal-weight.entity';
import { User } from 'src/auth/user.entity';
import { Care } from 'src/care/care.entity';
import { Vaccine } from 'src/vaccine/vaccine.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Animal extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true })
  name: string;

  @Column({ default: null, nullable: true })
  description: string;

  @Column({ type: 'date', default: null, nullable: true })
  age: string;

  @Column({ default: null, nullable: true })
  race: string;

  @Column({ type: 'date', default: null, nullable: true })
  reception_date: string;

  @Column({ type: 'date', default: null, nullable: true })
  shop_date: string;

  @Column('simple-array', { default: null, nullable: true })
  food: string[];

  @Column('simple-array', { default: null, nullable: true })
  amount_schedule: string;

  @Column('simple-array', { default: null, nullable: true })
  suplements: string;

  @Column('simple-array', { default: null, nullable: true })
  maintenance: string;

  @Column({ default: null, nullable: true })
  notes: string;

  @Column({ default: null, nullable: true })
  image: string;

  @Column({ default: null, nullable: true })
  technicalImage1: string;

  @Column({ default: null, nullable: true })
  technicalImage2: string;

  @Column({ default: null, nullable: true })
  technicalImage3: string;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @ManyToOne(
    () => User,
    user => user.animals,
  )
  user: User;

  @OneToMany(
    () => AnimalWeight,
    user => user.animal,
  )
  animalWeight: AnimalWeight[];

  @OneToMany(
    () => Vaccine,
    vaccine => vaccine.animal,
  )
  animalVaccine: Vaccine[];

  @OneToMany(
    () => Care,
    care => care.animal,
  )
  animalCare: Care[];

  @OneToMany(
    () => AnimalUser,
    entity => entity.animal,
    {
      cascade: true,
    },
  )
  public userAnimal!: AnimalUser[];
}
