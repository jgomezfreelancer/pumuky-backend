import { IsNotEmpty } from 'class-validator';

export class DeleteAnimalDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  user: number;
}
