import { IsNotEmpty } from 'class-validator';
import { CreateCareDto } from 'src/care/dto/create-care.dto';
import { CreateVaccineDto } from 'src/vaccine/dto/create-vaccine.dto';

interface IAnimalWeight {
  id?: number;
  description: string;
  type: string;
}

export interface ImageObject {
  data?: string;
  type?: string;
  name?: any;
}

export class CreateAnimalDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  age: string;

  @IsNotEmpty()
  race: string;

  @IsNotEmpty()
  weight: IAnimalWeight;

  @IsNotEmpty()
  shop_date: string;

  @IsNotEmpty()
  food: string[];

  @IsNotEmpty()
  suplements: string;

  @IsNotEmpty()
  maintenance: string;

  @IsNotEmpty()
  amount_schedule: string;

  @IsNotEmpty()
  notes: string;

  @IsNotEmpty()
  userId: number;

  @IsNotEmpty()
  reception_date: string;

  @IsNotEmpty()
  fileImage: ImageObject | undefined;

  @IsNotEmpty()
  techImage1: ImageObject | undefined;

  @IsNotEmpty()
  techImage2: ImageObject | undefined;

  @IsNotEmpty()
  techImage3: ImageObject | undefined;

  @IsNotEmpty()
  vaccines: CreateVaccineDto[];

  @IsNotEmpty()
  cares: CreateCareDto[];
}
