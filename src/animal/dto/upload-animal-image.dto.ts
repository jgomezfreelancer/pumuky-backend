import { IsNotEmpty } from 'class-validator';

export class UploadAnimalImageDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  file: { data: string; name: string; type: string } | undefined;
}
