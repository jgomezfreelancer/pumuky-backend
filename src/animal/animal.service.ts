import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from 'src/auth/user.repository';
import { getImageName } from 'src/utils/helpers';
import { Animal } from './animal.entity';
import { AnimalRepository } from './animal.repository';
import { CreateAnimalDto } from './dto/create-animal.dto';
import * as fs from 'fs';
import { UploadAnimalImageDto } from './dto/upload-animal-image.dto';
import { extname } from 'path';
import { AnimalWeightRepository } from 'src/animal-weight/animal-weight.repository';
import { VaccineRepository } from 'src/vaccine/vaccine.repository';
import { VaccineService } from 'src/vaccine/vaccine.service';
import { CareService } from 'src/care/care.service';
import { CareRepository } from 'src/care/care.repository';
import { DeleteAnimalDto } from './dto/delete-animal.dto';

@Injectable()
export class AnimalService {
  constructor(
    @InjectRepository(AnimalRepository)
    private repo: AnimalRepository,
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    @InjectRepository(AnimalWeightRepository)
    private animalWeightRepo: AnimalWeightRepository,
    @InjectRepository(CareRepository)
    private careRepo: CareRepository,
    @InjectRepository(VaccineRepository)
    private vaccineRepo: VaccineRepository,
    private vaccineService: VaccineService,
    private careService: CareService,
  ) {}

  async uploadFileImage(
    id: number,
    file: {
      data?: string;
      type?: string;
      name?: any;
    },
    position: number,
  ): Promise<any> {
    if (file) {
      const currentAnimal = await this.repo.findOne(id);
      const fileExtName = extname(file.name);
      const image = getImageName(`file-${position}-${file.name}`, id);
      console.log('fileExtName ', fileExtName);
      // if (currentAnimal.image) {
      //   await fs.unlinkSync(`src/images/${currentAnimal.image}`);
      // }
      const res = await fs.writeFile(
        `src/images/${image}`,
        file.data,
        'base64',
        function(err) {},
      );
      let field = {};
      if (position === 1) field = { technicalImage1: image };
      if (position === 2) field = { technicalImage2: image };
      if (position === 3) field = { technicalImage3: image };
      this.repo.update(id, field);
    }
  }
  async store(body: CreateAnimalDto): Promise<Animal> {
    const user = await this.userRepo.findOne(body.userId);
    const animal = await this.repo.store(body, user);
    this.animalWeightRepo.store({ ...body.weight, animal });
    if (body.techImage1) {
      this.uploadFileImage(animal.id, body.techImage1, 1);
    }
    if (body.techImage2) {
      this.uploadFileImage(animal.id, body.techImage2, 2);
    }
    if (body.techImage3) {
      this.uploadFileImage(animal.id, body.techImage3, 3);
    }
    if (!user.petsLimit) {
      await this.userRepo.update(user.id, { petsLimit: 1 });
    }
    if (body.vaccines) {
      for (const element of body.vaccines) {
        await this.vaccineService.store({ ...element, animalId: animal.id });
      }
    }

    if (body.cares) {
      for (const element of body.cares) {
        await this.careService.store({ ...element, animalId: animal.id });
      }
    }

    const entity = await this.repo.findOne({
      where: { id: animal.id },
      relations: ['animalVaccine', 'animalCare'],
    });
    return entity;
  }

  async update(body: CreateAnimalDto, id: number): Promise<any> {
    const {
      name,
      description,
      age,
      race,
      food,
      suplements,
      maintenance,
      notes,
      reception_date,
    } = body;
    const data = {
      name,
      description,
      age,
      race,
      food,
      suplements,
      maintenance,
      notes,
      reception_date,
    };
    await this.repo.update(id, data);
    if (body.techImage1) {
      this.uploadFileImage(id, body.techImage1, 1);
    }
    if (body.techImage2) {
      this.uploadFileImage(id, body.techImage2, 2);
    }
    if (body.techImage3) {
      this.uploadFileImage(id, body.techImage3, 3);
    }
    return this.repo.findOne({
      where: { id },
      relations: ['animalVaccine', 'animalCare', 'animalWeight'],
    });
  }

  async get(id: number): Promise<Animal> {
    const animal: any = await this.repo.findOne({
      where: { id },
      relations: ['animalVaccine', 'animalCare', 'animalWeight'],
    });
    if (animal.image) {
      const newImage = await this.getAnimalImage(animal.image);
      animal.image = newImage;
    }
    return animal;
  }

  async getAnimalImage(image: string): Promise<string | null> {
    if (image !== null) {
      try {
        const data: any = await fs.readFileSync(`./src/images/${image}`);
        const base64 = Buffer.from(data).toString('base64');
        const image64 = `data:image/png;base64,${base64}`;
        return image64;
      } catch (error) {
        return null;
      }
    }
    return null;
  }

  async getAnimalByUser(id: number): Promise<Animal[]> {
    const user = await this.userRepo.findOne({
      where: { id },
      relations: ['animals'],
    });
    const animals = await this.repo.find({
      where: { user },
      order: { description: 'ASC' },
    });
    for (const [index, item] of animals.entries()) {
      if (item.image) {
        const newImage = await this.getAnimalImage(item.image);
        animals[index].image = newImage;
      }
    }
    return animals;
  }

  async uploadImage(body: UploadAnimalImageDto): Promise<Animal> {
    const { id, file } = body;
    if (file) {
      const currentAnimal = await this.repo.findOne(id);
      const fileExtName = extname(file.name);
      const image = getImageName(file.name, id);
      console.log('fileExtName ', fileExtName);
      // if (currentAnimal.image) {
      //   await fs.unlinkSync(`src/images/${currentAnimal.image}`);
      // }
      const res = await fs.writeFile(
        `src/images/${image}`,
        file.data,
        'base64',
        function(err) {},
      );
      const updateres = await this.repo.update(id, { image });
      return this.repo.findOne(id);
    }
  }

  async removeAnimal(body: DeleteAnimalDto): Promise<Animal[]> {
    const { id, user } = body;
    await this.animalWeightRepo
      .createQueryBuilder()
      .delete()
      .where('animalId = :id', { id })
      .execute();

    await this.careRepo
      .createQueryBuilder()
      .delete()
      .where('animalId = :id', { id })
      .execute();

    await this.vaccineRepo
      .createQueryBuilder()
      .delete()
      .where('animalId = :id', { id })
      .execute();

    await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
    const animals = await this.repo.find({
      where: { user },
      order: { description: 'ASC' },
    });
    for (const [index, item] of animals.entries()) {
      if (item.image) {
        const newImage = await this.getAnimalImage(item.image);
        animals[index].image = newImage;
      }
    }
    return animals;
  }
}
