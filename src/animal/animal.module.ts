import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnimalUser } from 'src/animal-user/animal-user.entity';
import { AnimalUserRepository } from 'src/animal-user/animal-user.repository';
import { AnimalWeightRepository } from 'src/animal-weight/animal-weight.repository';
import { UserRepository } from 'src/auth/user.repository';
import { CareRepository } from 'src/care/care.repository';
import { CareService } from 'src/care/care.service';
import { VaccineRepository } from 'src/vaccine/vaccine.repository';
import { VaccineService } from 'src/vaccine/vaccine.service';
import { AnimalController } from './animal.controller';
import { AnimalRepository } from './animal.repository';
import { AnimalService } from './animal.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AnimalRepository,
      UserRepository,
      AnimalUserRepository,
      AnimalWeightRepository,
      VaccineRepository,
      CareRepository,
    ]),
  ],
  controllers: [AnimalController],
  providers: [AnimalService, VaccineService, CareService],
})
export class AnimalModule {}
