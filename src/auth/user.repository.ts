import { Repository, EntityRepository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { User } from './user.entity';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { SocialNetworkCredentialsDto } from './dto/social-network-singup-dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async signUp(
    authCredentialsDto: AuthCredentialsDto,
    locale: string,
  ): Promise<User> {
    const { username, password, first_name, last_name } = authCredentialsDto;

    const user = new User();
    user.username = username.toLowerCase();
    user.first_name = first_name;
    user.last_name = last_name;
    user.connected = true;
    user.first_time = true;
    user.salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(password, user.salt);
    user.lang = locale;

    try {
      const newUser: User = await user.save();
      return newUser;
    } catch (error) {
      if (error.code === '23505') {
        // duplicate username
        throw new ConflictException('Username already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async socialNetworkSignUp(
    authCredentialsDto: SocialNetworkCredentialsDto,
  ): Promise<User> {
    const { email, first_name, last_name, uuid } = authCredentialsDto;

    const user = new User();
    user.username = email;
    user.first_name = first_name;
    user.last_name = last_name;
    user.social_auth = 1;
    user.connected = true;
    user.first_time = true;
    user.uuid = uuid;
    try {
      return user.save();
    } catch (error) {
      if (error.code === '23505') {
        // duplicate username
        throw new ConflictException('Username already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async validateUserPassword(
    authCredentialsDto: AuthCredentialsDto,
  ): Promise<User> {
    const { username, password } = authCredentialsDto;
    const user = await this.findOne({ username: username.toLocaleLowerCase() });

    if (user && (await user.validatePassword(password))) {
      return user;
    } else {
      return null;
    }
  }

  async checkSocialNetworkUser(
    username: string,
    uuid: string | null = null,
  ): Promise<User> {
    if (username === 'authWithAppleId@test.com' && uuid) {
      const user = await this.findOne({ uuid });
      return user ? user : null;
    }

    if (username !== 'authWithAppleId@test.com' && uuid) {
      const user = await this.findOne({
        username: username.toLocaleLowerCase(),
      });
      if (user && uuid) {
        await this.update(user.id, { uuid });
      }
      return user ? user : null;
    }

    if (!uuid && username) {
      const user = await this.findOne({
        username: username.toLocaleLowerCase(),
      });
      return user ? user : null;
    }
  }

  public async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }
}
