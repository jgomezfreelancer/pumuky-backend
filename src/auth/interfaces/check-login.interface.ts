export interface ICheckLogin {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  social_auth: number;
  location: number;
  country_location: string;
  profile: number;
  help: number;
  language_id: number;
  nationality_id: number;
  fcm: string;
  latitude: string;
  longitude: string;
  age: number;
  address: string;
  cp: string;
  country_id: number;
  ranking: number;
}
