import { Injectable, UnauthorizedException, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import * as bcrypt from 'bcryptjs';
import { extname } from 'path';

import { UserRepository } from './user.repository';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtPayload } from './jwt-payload.interface';
import { User } from './user.entity';
import { AuthLoginCredentialsDto } from './dto/login-credentials.dto';
import { SocialNetworkCredentialsDto } from './dto/social-network-singup-dto';
import { AnimalRepository } from 'src/animal/animal.repository';
import { AnimalUserRepository } from 'src/animal-user/animal-user.repository';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { getImageName } from 'src/utils/helpers';
import { UploadUserProfileDto } from './dto/upload-user-profile.dto';
import { UpdateFcmUserDto } from './dto/update-fcm.dto';
import { UpdateUserSuscriptionDto } from './dto/update-user-suscription.dto.';

export interface CustomUserFields extends User {
  languages?: number[];
  nationalities?: number[];
  profileImage?: string | null;
  ranking?: string;
  userInterestKeys?: number[];
}
@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');

  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
    @InjectRepository(AnimalRepository)
    private animalRepository: AnimalRepository,
    @InjectRepository(AnimalUserRepository)
    private animalUserRepository: AnimalUserRepository,
  ) {}

  async signUp(
    authCredentialsDto: AuthCredentialsDto,
    locale: string,
  ): Promise<{ accessToken: string; user: User }> {
    const user = await this.userRepository.signUp(authCredentialsDto, locale);
    const payload: JwtPayload = { username: user.username };
    const accessToken = await this.jwtService.sign(payload);
    delete user.password;
    delete user.salt;

    this.logger.debug(
      `Generated JWT Token with payload ${JSON.stringify(payload)}`,
    );
    return { accessToken, user };
  }

  async signIn(
    authCredentialsDto: AuthLoginCredentialsDto,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    const user: CustomUserFields = await this.userRepository.validateUserPassword(
      authCredentialsDto,
    );

    if (!user) {
      throw new UnauthorizedException('Credenciales inválidas');
    }

    const payload: JwtPayload = { username: user.username };
    const accessToken = await this.jwtService.sign(payload);
    delete user.password;
    delete user.salt;
    if (user.picture) {
      const newImage = await this.getProfileImage(user.picture);
      user.picture = newImage;
    }
    await this.userRepository.update(user.id, { connected: true });
    this.logger.debug(
      `Generated JWT Token with payload ${JSON.stringify(payload)}`,
    );

    return { accessToken, user };
  }

  async refreshUser(id: number): Promise<User> {
    const user: CustomUserFields = await this.userRepository.findOne(id);
    delete user.password;
    delete user.salt;
    if (user.picture) {
      const newImage = await this.getProfileImage(user.picture);
      user.picture = newImage;
    }
    return user;
  }

  async socialNetworkSignUp(
    authCredentialsDto: SocialNetworkCredentialsDto,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    const { email, uuid } = authCredentialsDto;
    const existingUser: User = await this.userRepository.checkSocialNetworkUser(
      email,
      uuid,
    );

    const user: CustomUserFields = existingUser
      ? existingUser
      : await this.userRepository.socialNetworkSignUp(authCredentialsDto);
    const payload: JwtPayload = { username: user.username };
    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }
    if (user.picture) {
      const newImage = await this.getProfileImage(user.picture);
      user.picture = newImage;
    }
    const accessToken = await this.jwtService.sign(payload);
    delete user.password;
    delete user.salt;

    return { accessToken, user };
  }

  async getUsertoSingin(username: string): Promise<User> {
    const current: User = await this.userRepository.findOne({
      where: { username },
    });
    if (current.picture) {
      const newImage = await this.getProfileImage(current.picture);
      current.picture = newImage;
    }
    return current;
  }

  async getUserById(id: number): Promise<User> {
    const current: User = await this.userRepository.findOne(id);
    if (current.picture) {
      const newImage = await this.getProfileImage(current.picture);
      current.picture = newImage;
    }
    return current;
  }

  public async logout(username: string): Promise<boolean> {
    return true;
  }

  async getProfileImage(image: string): Promise<string | null> {
    if (image !== null) {
      try {
        const data: any = await fs.readFileSync(`./src/images/${image}`);
        const base64 = Buffer.from(data).toString('base64');
        const image64 = `data:image/png;base64,${base64}`;
        return image64;
      } catch (error) {
        return null;
      }
    }
    return null;
  }

  async updateUser(id: number, body: UpdateUserProfileDto): Promise<User> {
    const { first_name, last_name, birthday, password } = body;
    if (password) {
      const salt = await bcrypt.genSalt();
      const newPassword = await this.userRepository.hashPassword(
        password,
        salt,
      );
      await this.userRepository.update(id, { salt, password: newPassword });
    }

    const data = {
      first_name,
      last_name,
      birthday,
    };

    this.userRepository.update(id, data);
    const user = await this.userRepository.findOne(id);
    if (user.picture) {
      const newImage = await this.getProfileImage(user.picture);
      user.picture = newImage;
    }
    return user;
  }

  async uploadProfileImage(body: UploadUserProfileDto): Promise<User> {
    const { id, file } = body;
    if (file) {
      const currentUser = await this.userRepository.findOne(id);
      // const fileExtName = extname(file.name);
      const image = getImageName(file.name, id);
      if (currentUser.picture) {
        await fs.unlinkSync(`src/images/${currentUser.picture}`);
      }
      const res = await fs.writeFile(
        `src/images/${image}`,
        file.data,
        'base64',
        function(err) {},
      );
      await this.userRepository.update(id, { picture: image });
      return this.userRepository.findOne(id);
    }
  }

  async updateFcm(id: number, body: UpdateFcmUserDto): Promise<User> {
    const { fcm, timezone } = body;
    this.userRepository.update(id, { fcm, timezone });
    const user = await this.getUserById(id);
    this.logger.debug(`FCM updated to user ${user.username}`);
    return user;
  }

  async updateSuscription(body: UpdateUserSuscriptionDto): Promise<User> {
    const { id, isMember } = body;
    const currentUser = await this.userRepository.findOne(id);
    const counter = currentUser.petsLimit ? currentUser.petsLimit + 1 : 1;
    await this.userRepository.update(id, { isMember, petsLimit: counter });
    const user = await this.userRepository.findOne(id);
    if (user.picture) {
      const newImage = await this.getProfileImage(user.picture);
      user.picture = newImage;
    }

    return user;
  }
}
