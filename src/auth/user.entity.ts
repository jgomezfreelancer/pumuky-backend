import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Animal } from 'src/animal/animal.entity';
import { AnimalUser } from 'src/animal-user/animal-user.entity';
import { UserCalendar } from 'src/user-calendar/user-calendar.entity';

@Entity()
@Unique(['username'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  public username: string;

  @Column({ default: null, nullable: true })
  public first_name: string;

  @Column({ default: null, nullable: true })
  public last_name: string;

  @Column({ default: null, nullable: true })
  public password: string;

  @Column({ default: null, nullable: true })
  public salt: string;

  @Column({ default: null, nullable: true })
  public social_auth: number;

  @Column({ default: null, nullable: true })
  public location: number;

  @Column({ default: null, nullable: true })
  public country_location: string;

  @Column({ default: null, nullable: true })
  public profile: number;

  @Column({ default: null, nullable: true })
  public help: number;

  @Column({ default: null, nullable: true })
  public language_id: number;

  @Column({ default: null, nullable: true })
  public nationality_id: number;

  @Column({ default: null, nullable: true })
  public fcm: string;

  @Column({ default: null, nullable: true })
  public latitude: string;

  @Column({ default: null, nullable: true })
  public longitude: string;

  @Column({ default: null, nullable: true })
  public age: number;

  @Column({ default: null, nullable: true })
  public address: string;

  @Column({ default: null, nullable: true })
  public cp: string;

  @Column({ default: null, nullable: true })
  public country_id: number;

  @Column({ default: null, nullable: true })
  public picture: string;

  @Column({ default: null, nullable: true })
  public lang: string;

  @Column({ default: null, nullable: true })
  public connected: boolean;

  @Column({ default: null, nullable: true })
  public limit_distance: number;

  @Column({ default: null, nullable: true })
  public first_time: boolean;

  @Column({ default: null, nullable: true })
  public birthday: string;

  @Column({ default: null, nullable: true })
  public timezone: string;

  @Column({ default: null, nullable: true })
  public petsLimit: number;

  @Column({ default: null, nullable: true })
  public uuid: string;

  @OneToMany(
    () => AnimalUser,
    entity => entity.user,
    {
      cascade: true,
    },
  )
  public userAnimal!: Promise<AnimalUser[]>;

  @OneToMany(
    () => Animal,
    photo => photo.user,
  )
  animals: Animal[];

  @OneToMany(
    () => UserCalendar,
    userCalendar => userCalendar.user,
  )
  userCalendar: UserCalendar[];

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }

  @Column({ default: null, nullable: true })
  public isMember: boolean;
}
