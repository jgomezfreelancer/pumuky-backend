import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  Get,
  UseGuards,
  Req,
  UnauthorizedException,
  Param,
  Put,
} from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService, CustomUserFields } from './auth.service';
import { AuthLoginCredentialsDto } from './dto/login-credentials.dto';
import { SocialNetworkCredentialsDto } from './dto/social-network-singup-dto';
import { JwtAuthGuard } from './Guards/auth-jwt.guard';
import { JwtService } from '@nestjs/jwt';
import { User } from './user.entity';
import { Request } from 'express';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { UploadUserProfileDto } from './dto/upload-user-profile.dto';
import { UpdateFcmUserDto } from './dto/update-fcm.dto';
import { UpdateUserSuscriptionDto } from './dto/update-user-suscription.dto.';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private jwtService: JwtService,
  ) {}

  @Post('/signup')
  signUp(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
    @Req() req: Request,
  ): Promise<{ accessToken: string; user: User }> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.authService.signUp(authCredentialsDto, locale);
  }

  @Post('/social-signup')
  socialNetWorkSignUp(
    @Body(ValidationPipe) authCredentialsDto: SocialNetworkCredentialsDto,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    return this.authService.socialNetworkSignUp(authCredentialsDto);
  }

  @Post('/signin')
  signIn(
    @Body(ValidationPipe) authCredentialsDto: AuthLoginCredentialsDto,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    return this.authService.signIn(authCredentialsDto);
  }

  @Get('/check')
  @UseGuards(JwtAuthGuard)
  check(@Req() req: Request): Promise<User> {
    const { authorization: accessToken } = req.headers;
    try {
      const decoded = this.jwtService.verify(
        accessToken.replace('Bearer ', ''),
      );
      const user = this.authService.getUsertoSingin(decoded.username);
      return user;
    } catch (ex) {
      throw new UnauthorizedException();
    }
  }

  @Get('/logout/:token')
  async logout(@Param('token') token: string): Promise<boolean> {
    if (token) {
      const decoded = this.jwtService.decode(token.replace('Bearer ', ''));
      this.authService.logout(decoded['username']);
    }
    return true;
  }

  @Put('/update-user/:id')
  @UseGuards(JwtAuthGuard)
  updateUser(
    @Param('id') id: number,
    @Body() body: UpdateUserProfileDto,
  ): Promise<User> {
    return this.authService.updateUser(id, body);
  }

  @Post('/upload-image')
  async uploadImage(@Body() body: UploadUserProfileDto): Promise<User> {
    return this.authService.uploadProfileImage(body);
  }

  @Put('/update-fcm/:id')
  updateFcm(
    @Param('id') id: number,
    @Body() body: UpdateFcmUserDto,
  ): Promise<User> {
    return this.authService.updateFcm(id, body);
  }

  @Post('/update-suscription')
  @UseGuards(JwtAuthGuard)
  updateSuscription(@Body() body: UpdateUserSuscriptionDto): Promise<User> {
    return this.authService.updateSuscription(body);
  }

  @Get('/user-refresh/:id')
  @UseGuards(JwtAuthGuard)
  refreshUser(@Param('id') id: number): Promise<User> {
    return this.authService.refreshUser(id);
  }
}
