import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { JwtAuthGuard } from './Guards/auth-jwt.guard';

import { ENV } from '../env';
import { AnimalRepository } from 'src/animal/animal.repository';
import { AnimalUserRepository } from 'src/animal-user/animal-user.repository';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: ENV.jwt.secret,
      signOptions: {
        expiresIn: ENV.jwt.expiresIn,
      },
    }),
    TypeOrmModule.forFeature([
      UserRepository,
      AnimalRepository,
      AnimalUserRepository,
    ]),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, JwtAuthGuard],
  exports: [JwtStrategy, PassportModule],
})
export class AuthModule {}
