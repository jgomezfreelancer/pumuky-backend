import { IsString } from 'class-validator';

export class AuthCredentialsDto {
  @IsString()
  username: string;

  @IsString()
  first_name?: string;

  @IsString()
  last_name?: string;

  @IsString()
  password: string;
}
