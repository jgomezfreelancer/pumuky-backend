import { IsNumber, IsString } from 'class-validator';

export class UpdateLanguagesNationalitiesDto {
  @IsNumber()
  nationalities: number[];
  
  @IsNumber()
  languages: number[];

  @IsNumber()
  nationality_id: number;
}
