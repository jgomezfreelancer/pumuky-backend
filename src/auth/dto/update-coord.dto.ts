import { IsString } from 'class-validator';

export class UpdateCoordUserDto {
  @IsString()
  latitude: string;
  
  @IsString()
  longitude: string;
}
