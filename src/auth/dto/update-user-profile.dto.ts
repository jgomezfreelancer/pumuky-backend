import { IsString } from 'class-validator';

export class UpdateUserProfileDto {
  @IsString()
  first_name: string;

  @IsString()
  last_name: string;

  @IsString()
  birthday: string;

  @IsString()
  password: string;
}
