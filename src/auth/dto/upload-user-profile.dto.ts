import { IsNotEmpty } from 'class-validator';

export class UploadUserProfileDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  file: { data: string; name: string; type: string };
}
