import { IsString } from 'class-validator';

export class UpdateFcmUserDto {
  @IsString()
  fcm: string;

  @IsString()
  timezone: string;
}
