import { IsNumber, IsString } from 'class-validator';

export class SendHelpUserDto {
  @IsNumber()
  id: number;

  @IsNumber()
  km: number;

  @IsString()
  message: string;

  @IsNumber()
  interests: number[];
}
