import { IsString } from 'class-validator';

export class UpdateUserSuscriptionDto {
  @IsString()
  id: string;

  @IsString()
  isMember: boolean;
}
