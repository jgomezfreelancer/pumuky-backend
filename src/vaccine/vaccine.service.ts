import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import { extname } from 'path';
import { AnimalRepository } from 'src/animal/animal.repository';
import { getImageName } from 'src/utils/helpers';

import { CreateVaccineDto } from './dto/create-vaccine.dto';
import { DeleteVaccineDto } from './dto/delete-vaccine.dto';
import { Vaccine } from './vaccine.entity';
import { VaccineRepository } from './vaccine.repository';

@Injectable()
export class VaccineService {
  constructor(
    @InjectRepository(VaccineRepository)
    private repo: VaccineRepository,
    @InjectRepository(AnimalRepository)
    private animalRepo: AnimalRepository,
  ) { }

  async getImage(image: string): Promise<string | null> {
    if (image !== null) {
      try {
        const data: any = await fs.readFileSync(`./src/images/${image}`);
        const base64 = Buffer.from(data).toString('base64');
        const image64 = `data:image/png;base64,${base64}`;
        return image64;
      } catch (error) {
        return null;
      }
    }
    return null;
  }

  async uploadImage(
    id: number,
    file: {
      data?: string;
      type?: string;
      name?: any;
    },
  ): Promise<any> {
    if (file) {
      const fileExtName = extname(file.name);
      const image = getImageName(file.name, id);
      const res = await fs.writeFile(
        `src/images/${image}`,
        file.data,
        'base64',
        function (err) { },
      );
      this.repo.update(id, { image });
    }
  }

  async store(body: CreateVaccineDto): Promise<Vaccine[]> {
    const { description, date, fileImage, note, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    const vaccine = new Vaccine();
    vaccine.description = description;
    vaccine.date = date;
    vaccine.note = note;
    vaccine.animal = animal;
    const result = await vaccine.save();
    if (fileImage) {
      await this.uploadImage(result.id, fileImage);
    }
    const vaccines = await this.repo.find({ where: { animal } });
    for (const [index, item] of vaccines.entries()) {
      if (item.image) {
        const newImage = await this.getImage(item.image);
        vaccines[index].image = newImage;
      }
    }
    return vaccines;
  }

  async getVaccineByAnimal(id: number): Promise<Vaccine[]> {
    const animal = await this.animalRepo.findOne(id);
    const vaccines = await this.repo.find({ where: { animal } });
    for (const [index, item] of vaccines.entries()) {
      if (item.image) {
        const newImage = await this.getImage(item.image);
        vaccines[index].image = newImage;
      }
    }
    return vaccines;
  }

  async update(id: number, body: CreateVaccineDto): Promise<Vaccine[]> {
    const { description, date, fileImage, note, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    const data = {
      description,
      date,
      note,
    };
    await this.repo.update(id, data);
    if (fileImage) {
      await this.uploadImage(id, fileImage);
    }
    const vaccines = await this.repo.find({ where: { animal } });
    for (const [index, item] of vaccines.entries()) {
      if (item.image) {
        const newImage = await this.getImage(item.image);
        vaccines[index].image = newImage;
      }
    }
    return vaccines;
  }

  async delete(body: DeleteVaccineDto): Promise<Vaccine[]> {
    const { id, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
    const vaccines = await this.repo.find({ where: { animal } });
    for (const [index, item] of vaccines.entries()) {
      if (item.image) {
        const newImage = await this.getImage(item.image);
        vaccines[index].image = newImage;
      }
    }
    return vaccines;
  }
}
