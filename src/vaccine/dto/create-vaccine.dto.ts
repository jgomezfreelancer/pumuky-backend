import { IsNotEmpty } from 'class-validator';
import { ImageFileObject } from 'src/interfaces';

export class CreateVaccineDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  fileImage: ImageFileObject | undefined;

  @IsNotEmpty()
  note: string;

  @IsNotEmpty()
  animalId: number;
}
