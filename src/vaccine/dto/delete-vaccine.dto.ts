import { IsNotEmpty } from 'class-validator';

export class DeleteVaccineDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  animalId: number;
}
