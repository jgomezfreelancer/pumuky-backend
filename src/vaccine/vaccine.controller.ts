import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { CreateVaccineDto } from './dto/create-vaccine.dto';
import { DeleteVaccineDto } from './dto/delete-vaccine.dto';
import { Vaccine } from './vaccine.entity';
import { VaccineService } from './vaccine.service';

@Controller('vaccine')
export class VaccineController {
  constructor(private service: VaccineService) {}
  @Post('/')
  async store(@Body() body: CreateVaccineDto): Promise<Vaccine[]> {
    return this.service.store(body);
  }

  @Put('/:id')
  async update(
    @Body() body: CreateVaccineDto,
    @Param('id') id: number,
  ): Promise<Vaccine[]> {
    return this.service.update(id, body);
  }

  @Post('/delete')
  async delete(@Body() body: DeleteVaccineDto): Promise<Vaccine[]> {
    return this.service.delete(body);
  }

  @Get('/get-by-animal/:id')
  async getVaccineByAnimal(@Param('id') id: number): Promise<Vaccine[]> {
    return this.service.getVaccineByAnimal(id);
  }
}
