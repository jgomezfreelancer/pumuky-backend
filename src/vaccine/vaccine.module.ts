import { Module } from '@nestjs/common';
import { VaccineService } from './vaccine.service';
import { VaccineController } from './vaccine.controller';
import { AnimalRepository } from 'src/animal/animal.repository';
import { VaccineRepository } from './vaccine.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([VaccineRepository, AnimalRepository])],
  providers: [VaccineService],
  controllers: [VaccineController],
})
export class VaccineModule {}
