import { EntityRepository, Repository } from 'typeorm';
import { Vaccine } from './vaccine.entity';

@EntityRepository(Vaccine)
export class VaccineRepository extends Repository<Vaccine> {}
