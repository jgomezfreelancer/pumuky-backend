import { Animal } from 'src/animal/animal.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Vaccine extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true })
  description: string;

  @Column({ type: 'date', default: null, nullable: true })
  date: string;

  @Column({ default: null, nullable: true })
  image: string;

  @Column({ default: null, nullable: true })
  note: string;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @ManyToOne(
    () => Animal,
    animal => animal.animalVaccine,
  )
  animal: Animal;
}
