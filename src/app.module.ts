import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { typeOrmConfig } from './config/typeorm.config';
import { AnimalModule } from './animal/animal.module';
import { RaceModule } from './race/race.module';
import { AnimalAgeModule } from './animal-age/animal-age.module';
import { MulterModule } from '@nestjs/platform-express';
import { EmailModule } from './email/email.module';
import { AnimalUserModule } from './animal-user/animal-user.module';
import { AnimalWeightModule } from './animal-weight/animal-weight.module';
import { VaccineModule } from './vaccine/vaccine.module';
import { CareModule } from './care/care.module';
import { UserCalendarModule } from './user-calendar/user-calendar.module';
import { ReminderModule } from './reminder/reminder.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    MulterModule.register({
      dest: './src/images',
    }),
    TypeOrmModule.forRoot(typeOrmConfig),
    AuthModule,
    AnimalModule,
    RaceModule,
    AnimalAgeModule,
    EmailModule,
    AnimalUserModule,
    AnimalWeightModule,
    VaccineModule,
    CareModule,
    UserCalendarModule,
    ReminderModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
