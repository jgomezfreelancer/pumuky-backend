import { Test, TestingModule } from '@nestjs/testing';
import { UserCalendarController } from './user-calendar.controller';

describe('UserCalendarController', () => {
  let controller: UserCalendarController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserCalendarController],
    }).compile();

    controller = module.get<UserCalendarController>(UserCalendarController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
