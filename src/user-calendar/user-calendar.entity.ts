import { Animal } from 'src/animal/animal.entity';
import { User } from 'src/auth/user.entity';
import { Reminder } from 'src/reminder/reminder.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class UserCalendar extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'date', default: null, nullable: true })
  date: string;

  @Column({ default: null, nullable: true })
  notification_send: boolean;

  /* Field to valitate the day before to send push notification */
  @Column({ default: null, nullable: true })
  day_before: boolean;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @ManyToOne(
    () => User,
    animal => animal.userCalendar,
  )
  user: User;

  @OneToMany(
    () => Reminder,
    calendarReminders => calendarReminders.userCalendar,
  )
  reminders: Reminder[];
}
