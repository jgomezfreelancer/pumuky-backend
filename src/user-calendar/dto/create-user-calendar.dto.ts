import { IsNotEmpty } from 'class-validator';

export class CreateUserCalendarDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  userId: number;
}
