import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from 'src/auth/user.repository';
import { CreateUserCalendarDto } from './dto/create-user-calendar.dto';
import { UserCalendar } from './user-calendar.entity';
import { UserCalendarRepository } from './user-calendar.repository';

@Injectable()
export class UserCalendarService {
  constructor(
    @InjectRepository(UserCalendarRepository)
    private repo: UserCalendarRepository,
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
  ) {}

  async get(userId: number): Promise<UserCalendar[]> {
    const user = await this.userRepo.findOne(userId);
    return this.repo.find({ where: { user }, relations: ['reminders'] });
  }

  async store(body: CreateUserCalendarDto): Promise<UserCalendar> {
    const { date, userId } = body;
    const user = await this.userRepo.findOne(userId);
    const reminder = new UserCalendar();
    reminder.date = date;
    reminder.user = user;
    return reminder.save();
  }
}
