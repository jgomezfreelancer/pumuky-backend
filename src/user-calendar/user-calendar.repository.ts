import { EntityRepository, Repository } from 'typeorm';
import { UserCalendar } from './user-calendar.entity';

@EntityRepository(UserCalendar)
export class UserCalendarRepository extends Repository<UserCalendar> {}
