import { Module } from '@nestjs/common';
import { UserCalendarService } from './user-calendar.service';
import { UserCalendarController } from './user-calendar.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserCalendarRepository } from './user-calendar.repository';
import { UserRepository } from 'src/auth/user.repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserCalendarRepository, UserRepository])],
  providers: [UserCalendarService],
  controllers: [UserCalendarController],
})
export class UserCalendarModule {}
