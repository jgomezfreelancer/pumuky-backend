import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CreateUserCalendarDto } from './dto/create-user-calendar.dto';
import { UserCalendar } from './user-calendar.entity';
import { UserCalendarService } from './user-calendar.service';

@Controller('user-calendar')
export class UserCalendarController {
  constructor(private service: UserCalendarService) {}

  @Get('/get-by-user/:id')
  async get(@Param('id') id: number): Promise<UserCalendar[]> {
    return this.service.get(id);
  }

  @Post('/')
  async store(@Body() body: CreateUserCalendarDto): Promise<UserCalendar> {
    return this.service.store(body);
  }
}
