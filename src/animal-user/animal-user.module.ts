import { Module } from '@nestjs/common';
import { AnimalUserService } from './animal-user.service';
import { AnimalUserController } from './animal-user.controller';

@Module({
  providers: [AnimalUserService],
  controllers: [AnimalUserController]
})
export class AnimalUserModule {}
