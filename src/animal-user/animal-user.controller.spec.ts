import { Test, TestingModule } from '@nestjs/testing';
import { AnimalUserController } from './animal-user.controller';

describe('AnimalUserController', () => {
  let controller: AnimalUserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnimalUserController],
    }).compile();

    controller = module.get<AnimalUserController>(AnimalUserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
