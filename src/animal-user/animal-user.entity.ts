import {
  Entity,
  Column,
  ManyToOne,
  PrimaryGeneratedColumn,
  BaseEntity,
} from 'typeorm';
import { User } from 'src/auth/user.entity';
import { Animal } from 'src/animal/animal.entity';

@Entity()
export class AnimalUser extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ default: null, nullable: true })
  public userId!: number;

  @Column({ default: null, nullable: true })
  public animalId!: number;

  @ManyToOne(
    () => User,
    entity => entity.userAnimal,
  )
  public user!: User;

  @ManyToOne(
    () => Animal,
    entity => entity.userAnimal,
  )
  public animal!: Animal;
}
