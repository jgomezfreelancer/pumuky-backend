import { Test, TestingModule } from '@nestjs/testing';
import { AnimalUserService } from './animal-user.service';

describe('AnimalUserService', () => {
  let service: AnimalUserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnimalUserService],
    }).compile();

    service = module.get<AnimalUserService>(AnimalUserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
