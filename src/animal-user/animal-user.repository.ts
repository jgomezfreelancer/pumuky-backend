import { EntityRepository, Repository } from 'typeorm';
import { AnimalUser } from './animal-user.entity';

@EntityRepository(AnimalUser)
export class AnimalUserRepository extends Repository<AnimalUser> {}
