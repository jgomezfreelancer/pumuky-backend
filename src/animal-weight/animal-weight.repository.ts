import { EntityRepository, Repository } from 'typeorm';
import { AnimalWeight } from './animal-weight.entity';
import { CreateAnimalWeightDto } from './dto/create-animal-weight.dto';

@EntityRepository(AnimalWeight)
export class AnimalWeightRepository extends Repository<AnimalWeight> {
  async store(body: CreateAnimalWeightDto): Promise<AnimalWeight> {
    const { description, type, animal } = body;

    const nationality = new AnimalWeight();
    nationality.description = description;
    nationality.type = type;
    nationality.animal = animal;
    return nationality.save();
  }
}
