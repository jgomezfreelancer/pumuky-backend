import { Test, TestingModule } from '@nestjs/testing';
import { AnimalWeightController } from './animal-weight.controller';

describe('AnimalWeightController', () => {
  let controller: AnimalWeightController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnimalWeightController],
    }).compile();

    controller = module.get<AnimalWeightController>(AnimalWeightController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
