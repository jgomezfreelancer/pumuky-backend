import { Animal } from 'src/animal/animal.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  ManyToOne,
} from 'typeorm';

@Entity()
export class AnimalWeight extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true })
  description: string;

  @Column({ default: null, nullable: true })
  type: string;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @ManyToOne(
    () => Animal,
    animal => animal.animalWeight,
  )
  animal: Animal;
}
