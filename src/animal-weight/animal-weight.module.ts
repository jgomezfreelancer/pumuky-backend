import { Module } from '@nestjs/common';
import { AnimalWeightService } from './animal-weight.service';
import { AnimalWeightController } from './animal-weight.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnimalWeightRepository } from './animal-weight.repository';
import { AnimalRepository } from 'src/animal/animal.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([AnimalWeightRepository, AnimalRepository]),
  ],
  providers: [AnimalWeightService],
  controllers: [AnimalWeightController],
})
export class AnimalWeightModule {}
