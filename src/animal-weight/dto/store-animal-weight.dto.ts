import { IsNotEmpty } from 'class-validator';

export class StoreAnimalWeightDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  animalId: number;
}
