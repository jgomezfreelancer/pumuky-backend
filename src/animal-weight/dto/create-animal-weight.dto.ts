import { IsNotEmpty } from 'class-validator';
import { Animal } from 'src/animal/animal.entity';

export class CreateAnimalWeightDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  animal: Animal;
}
