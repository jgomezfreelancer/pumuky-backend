import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AnimalRepository } from 'src/animal/animal.repository';
import { AnimalWeight } from './animal-weight.entity';
import { AnimalWeightRepository } from './animal-weight.repository';
import { StoreAnimalWeightDto } from './dto/store-animal-weight.dto';

@Injectable()
export class AnimalWeightService {
  constructor(
    @InjectRepository(AnimalWeightRepository)
    private repo: AnimalWeightRepository,
    @InjectRepository(AnimalRepository)
    private animalRepo: AnimalRepository,
  ) {}

  async store(body: StoreAnimalWeightDto): Promise<AnimalWeight[]> {
    const { description, type, animalId } = body;
    const animal = await this.animalRepo.findOne(animalId);
    const animalWeight = new AnimalWeight();
    animalWeight.description = description;
    animalWeight.type = type;
    animalWeight.animal = animal;
    await animalWeight.save();
    return this.repo.find({ where: { animal }, order: { created: 'DESC' } });
  }
}
