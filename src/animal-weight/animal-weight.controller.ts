import { Body, Controller, Post } from '@nestjs/common';
import { AnimalWeight } from './animal-weight.entity';
import { AnimalWeightService } from './animal-weight.service';
import { StoreAnimalWeightDto } from './dto/store-animal-weight.dto';

@Controller('animal-weight')
export class AnimalWeightController {
  constructor(private service: AnimalWeightService) {}

  @Post('/')
  async store(@Body() body: StoreAnimalWeightDto): Promise<AnimalWeight[]> {
    return this.service.store(body);
  }
}
