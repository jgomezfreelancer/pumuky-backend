import { Test, TestingModule } from '@nestjs/testing';
import { AnimalWeightService } from './animal-weight.service';

describe('AnimalWeightService', () => {
  let service: AnimalWeightService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnimalWeightService],
    }).compile();

    service = module.get<AnimalWeightService>(AnimalWeightService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
